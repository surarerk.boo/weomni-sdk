/*
 * Public API Surface of project-management
 */

export * from './lib/project-management.service';
export * from './lib/project-management.component';
export * from './lib/project-management.module';
