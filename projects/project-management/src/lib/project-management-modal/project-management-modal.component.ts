import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import { Project } from '../../model/project.model';

@Component({
    selector: 'jhi-project-management-modal',
    templateUrl: './project-management-modal.component.html',
    styles: ['./project-management-modal.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class ProjectManagementModalComponent {
    @Input() projects?: Project[];

    @Input() selectedProjectId?: string;
    @Output() selectedProjectIdChange = new EventEmitter<any>();

    projectListClasses(project: Project): string {
        if (project.id === this.selectedProjectId) {
            return 'bg-blue-100'
        }

        return 'hover:bg-blue-50';
    }

    selectProject(projectId: string): void {
        this.selectedProjectId = projectId;
        this.emitProjectChange(projectId);
    }

    private emitProjectChange(projectId: string): void {
        this.selectedProjectIdChange.emit(projectId);
    }
}
