import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { Project } from '../model/project.model';
import * as _ from 'lodash';
@Component({
    selector: 'jhi-lib-project-management',
    templateUrl: './project-management.component.html',
    styleUrls: ['./project-management.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class LibProjectManagementComponent implements OnChanges {
  @Input() projects?: Project[];

  @Input() selectedProjectId?: string;
  @Output() selectedProjectIdChange = new EventEmitter<string>();

  selectedProjectName?: string;
  isModalOpen = false;

  ngOnChanges(changes: SimpleChanges): void {
    const projects = _.get(changes, 'projects.currentValue', []) as Project[];
    const selectedProjectId = _.get(changes, 'selectedProjectId.currentValue', '') as string;

    if (projects.length > 0) {
      this.projects = projects;
    }

    if (!selectedProjectId) {
      this.selectedProjectId = _.get(projects, '[0].id', '');
      this.selectedProjectName = _.get(projects, '[0].name', '');
    } else {
      this.selectedProjectId = selectedProjectId;
      this.selectedProjectName = this.projects?.find(p => p.id === selectedProjectId)?.name;
    }
  }

  toggleModal(): void {
    this.isModalOpen = !this.isModalOpen;
  }

  onSelectedProjectIdChange(event: string): void {
    this.selectedProjectId = event;
    this.emitSelectedProjectIdChange();
    this.toggleModal();
  }

  private emitSelectedProjectIdChange(): void {
    this.selectedProjectIdChange.emit(this.selectedProjectId);
  }
}
