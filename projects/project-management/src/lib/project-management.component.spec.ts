import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LibProjectManagementComponent } from './project-management.component';

describe('ProjectManagementComponent', () => {
  let component: LibProjectManagementComponent;
  let fixture: ComponentFixture<LibProjectManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LibProjectManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LibProjectManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
