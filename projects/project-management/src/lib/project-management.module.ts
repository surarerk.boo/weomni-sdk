import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibProjectManagementComponent } from './project-management.component';
import { ProjectManagementModalComponent } from './project-management-modal/project-management-modal.component';

@NgModule({
  declarations: [
    LibProjectManagementComponent,
    ProjectManagementModalComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LibProjectManagementComponent,
    ProjectManagementModalComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProjectManagementModule { }
